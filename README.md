![Build Status](https://gitlab.com/arragon/blackbook/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Personal Jekyll website using GitLab Pages.

---
