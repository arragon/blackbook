var gulp        = require('gulp'),
    plumber     = require('gulp-plumber'),
    stylus      = require('gulp-stylus'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    jeet        = require('jeet'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss'),
    prefixer    = require('autoprefixer-stylus'),
    imagemin    = require('gulp-imagemin'),
    cp          = require('child_process');
    jshint      = require('gulp-jshint');


var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
    realFavicon.generateFavicon({
	masterPicture: 'src/img/master_icon.jpeg',
	dest: 'assets/img/icons',
	iconsPath: '/assets/img/icons',
	design: {
	    ios: {
		pictureAspect: 'noChange',
		assets: {
		    ios6AndPriorIcons: true,
		    ios7AndLaterIcons: true,
		    precomposedIcons: true,
		    declareOnlyDefaultIcon: true
		}
	    },
	    desktopBrowser: {},
	    windows: {
		pictureAspect: 'noChange',
		backgroundColor: '#603cba',
		onConflict: 'override',
		assets: {
		    windows80Ie10Tile: true,
		    windows10Ie11EdgeTiles: {
			small: false,
			medium: true,
			big: true,
			rectangle: false
		    }
		}
	    },
	    androidChrome: {
		pictureAspect: 'shadow',
		themeColor: '#ffffff',
		manifest: {
		    name: 'Schwarzes Buch',
		    display: 'standalone',
		    orientation: 'notSet',
		    onConflict: 'override',
		    declared: true
		},
		assets: {
		    legacyIcon: false,
		    lowResolutionIcons: true
		}
	    },
	    safariPinnedTab: {
		pictureAspect: 'blackAndWhite',
		threshold: 70.46875,
		themeColor: '#341b54'
	    }
	},
	settings: {
	    compression: 1,
	    scalingAlgorithm: 'Mitchell',
	    errorOnImageTooSmall: false
	},
	versioning: {
	    paramName: 'v',
	    paramValue: 'PYEkQ78nyA'
	},
	markupFile: FAVICON_DATA_FILE
    }, function() {
	done();
    });
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
    var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
    realFavicon.checkForUpdates(currentVersion, function(err) {
	if (err) {
	    throw err;
	}
    });
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

/**
 * Stylus task
 */
gulp.task('stylus', function(){
    gulp.src('src/styl/main.styl')
	.pipe(plumber())
	.pipe(stylus({
	    use:[koutoSwiss(), prefixer(), jeet(),rupture()],
	    compress: true
	}))
	.pipe(gulp.dest('_site/assets/css/'))
	.pipe(gulp.dest('assets/css'))
});

/**
 * Javascript Task
 */
gulp.task('js', function(){
    return gulp.src('src/js/**/*.js')
	.pipe(plumber())
	.pipe(concat('main.js'))
	.pipe(uglify())
	.pipe(gulp.dest('assets/js/'))
	.pipe(gulp.dest('_site/assets/js/'))
});

/**
 * Imagemin Task
 */
gulp.task('imagemin', function() {
    return gulp.src('src/img/**/*.{jpg,png,gif,jpeg}')
	.pipe(plumber())
	.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
	.pipe(gulp.dest('assets/img/'));
});

/**
 * Watch stylus files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */
gulp.task('watch', function () {
    gulp.watch('src/styl/**/*.styl', ['stylus']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/img/**/*.{jpg,png,gif,jpeg}', ['imagemin']);
});

/**
 * Default task, running just `gulp` will compile the sass &
 * watch files.
 */
gulp.task('default', ['generate-favicon', 'lint', 'js', 'imagemin', 'stylus']);
