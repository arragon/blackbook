---
layout: post
title: "Emacs sinnvoll einstellen"
serie: emacs
categories:
- Emacs für den Alltag
tags:
- emacs
---

Folgende Einstellungen sind meist nur aus dem großartigen [EmacsWiki] zusammengeklaut :smirk:

```emacs-lisp
;; keinen temporären Müll zurücklassen
(setq
 backup-by-copying t      ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves"))    ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)       ; use versioned backups
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Mehr Packages für Emacs
(require 'package)
(add-to-list 'package-archives
	     '("melpa-stable" . "http://stable.melpa.org/packages/") t)
;; PuTTY fix. Ugly. Bad. But it works. (Good)
(define-key global-map [select] 'end-of-line)
;; Fenster-Teilung ändern
(defun window-toggle-split-direction ()
  "Switch window split from horizontally to vertically, or vice versa. i.e. change right window to bottom, or change bottom window to right."
  (interactive)
  (require 'windmove)
  (let ((done))
    (dolist (dirs '((right . down) (down . right)))
      (unless done
	(let* ((win (selected-window))
	       (nextdir (car dirs))
	       (neighbour-dir (cdr dirs))
	       (next-win (windmove-find-other-window nextdir win))
	       (neighbour1 (windmove-find-other-window neighbour-dir win))
	       (neighbour2 (if next-win (with-selected-window next-win
					  (windmove-find-other-window neighbour-dir next-win)))))
	  ;;(message "win: %s\nnext-win: %s\nneighbour1: %s\nneighbour2:%s" win next-win neighbour1 neighbour2)
	  (setq done (and (eq neighbour1 neighbour2)
			  (not (eq (minibuffer-window) next-win))))
	  (if done
	      (let* ((other-buf (window-buffer next-win)))
		(delete-window next-win)
		(if (eq nextdir 'right)
		    (split-window-vertically)
		  (split-window-horizontally))
		(set-window-buffer (windmove-find-other-window neighbour-dir) other-buf))))))))
;; Tastenkombination für Magit
(global-set-key (kbd "C-x g") 'magit-status)
```



[EmacsWiki]: https://www.emacswiki.org/
