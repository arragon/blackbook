---
layout: post
title: "Ideen für gruselige Szenen im Rollenspiel"
serie: cthulhu
categories:
- Call of Cthulhu
tags:
- Cthulhu
date: 2016-12-12 11:08:00 +0100
---

* Du hörst, dass jemand gegen eine Scheibe klopft.  Als du zum Fenster
  hinausschaust, hörst du es wieder... vom Spiegel hinter dir.
* Als du in den Spiegel schaust, erblickst du nur dein Spiegelbild.
  Es fällt dir nichts merkwürdiges auf, bis du dein Gegenüber blinzeln
  siehst.
* Die Statue steht noch am gleichen Platz, doch nachdem du geblinzelt
  hast, schaut sie dich an. [Weeping
  Angel](http://tardis.wikia.com/wiki/Weeping_Angel)
* Dir fehlen drei Tage... und eine Erklärung für die Narbe in deinem
  Nacken.
* Du greifst nach dem Lichtschalter, aber da ist bereits eine Hand.
* Es gibt nur eine Lichtquelle, aber du wirfst zwei Schatten... bis
  einer von ihnen es bemerkt.

