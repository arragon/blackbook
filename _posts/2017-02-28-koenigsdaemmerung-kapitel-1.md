---
layout: post
title: "Königsdämmerung - Kapitel 1"
serie: cthulhu
categories:
- Königsdämmerung
- Call of Cthulhu
tags:
- Cthulhu
date: 2017-02-28 10:45:00 +0100
---

# Charakterentwicklung / ~erstellung
* Nochmal anhand der *Fertigkeitsproben* aus dem Prolog überprüfen welche Steigerungen möglich sind

|         Skill          | Estelle | Mortimer | Claire | Patrick |
| :--------------------: | :-----: | :------: | :----: | :-----: |
|           BI           |         |    X     |        |         |
|  Verborgenes Erkennen  |    X    |          |        |         |
|           IN           |    X    |    X     |   X    |    X    |
|       Ausweichen       |    X    |          |        |         |
| Nahkampf (Handgemenge) |         |          |        |    X    |
|       Überreden        |         |          |   X    |         |
|        Horchen         |    X    |          |        |         |

* Alle Charaktere heilen 1 TP pro Tag

## Estelle Porter (Wulf)
* Beeinflussung der Träume durch das *gelbe Zeichen* 
* Kontakt zu *Talbot Estus* -- Einladung wurde ausgesprochen

## Mortimer Henceworth (Magdalena)
* seine Frau fühlt sich zunehmend übergangen
* erhält Brief von *Dr. Highsmith*

## Claire Starling (Karl)
* **Finanzkraft** steigern um **1W3**
 * freundschaftliche Beziehung mit Schauspielerin *Jean Hewart* führte zu mehr Besuchern ihrer Auftritte
 * Beginn einer geschäftlichen Kooperation mit Patrick -- Auftritte im Pub
* **Buchhaltung** verringtert sich im *30 Punkte*
* **Handwerk/Kunst** verbessert sich um *30 Punkte*
* Gesangslehrer für **1W8 Tage**, danach nochmalige Steigerung von **Handwerk/Kunst** um **1W10**

## Patrick (John)
* **Finanzkraft** steigern um *1 Punkt* wegen Geschäftsbeziehung mit Claire

# Gedächtnisstütze

* **Wissen** kann mit erfolgreicher **BI**-Probe zu Tage kommen
* **Insiderwissen** erlangt man mit erfolgreicher **spezieller Fertigkeitsprobe** (bsw. Überzeugen)

# Kapitel 1

***(Tagebuch S. 12)*** für Aufzeichnungne

### Sa 19. Okt 1929

* Brief von Dr. Highsmith an Mortimer ***(Tagebuch S. 14/15)***

### Mo 28. Okt 1929 

* *Schauplatz*: Great Western Hotel, Endbahnhof Paddington der Great Western Railway
* gemütliches Hotel, mittlerer Preiskategorie, per Underground gut zu erreichen
* Dr. Highsmith kommt herunter und spricht über Wetter bis Tee serviert ist
* mit jeder Vorgehensweise einverstanden, solange Diskretion gewahrt wird
* **Investigatoren müssen Vertraulichkeit zusichern**
* Highsmith
  *  erzählt von Roby, seiner Familie und Arzt
  * sieht keinen Grund die 2 Jahre vorläufige Unterbringung in St. Agnes zu verlängern
  * will Roby unter Medikamentierung entlassen
  * würde gerne wissen, warum sich Familie und Arzt gegen Entlassung stemmen
* Investigatoren sollen mit Graham und Dr. Trollope sprechen - Empfehlungsschreiben schickt Highsmith
* Rückreise nach Hereford am 30. Okt. geplant
* Einzelheiten im Gespräch siehe **S. 28**
* *Nach dem Gespräch*: **schwierige Psychologie**-Probe: Highsmith verschweigt noch ein anderes Motiv, warum er so ein Interesse an dem Fall Roby hat

###  Zwischenzeit: Recherche

* *Zeitungsarchiv* mittel **Bilbiotheksnutzung**: 2 Artikel ***(Tagebuch S. 16)***
* *Kirche* St. Peter's Church: Gleicher Todestag für Herbert und Georgina - 14. Okt. 1926
* *Bestatter* Ames Funeral Home: Ablehnend, **Finanzkraft, Charme oder. Überzeugen**: Leichen "recht gräulich" zugerichtet und völlig ausgeblutet

### Mi 30. Okt 1929 10:20 Uhr

* *Paddington Station* Zug nach Bristol -> Hereford
* Highsmith hat kaum Gesprächsstoff äußer aktuelle Themen der Psychoanalyse
* bei der Ankunft am späten Nachmittag holt ein Fahrer ab
* 3 Zimmer in Nervenheilanstalt
* 2 Zimmer im lokalen Pub